const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
  'public/lib/bootstrap/css/bootstrap.min.css',
  'https://fonts.googleapis.com/css?family=Lato',
  'https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900',
  'https://fonts.googleapis.com/css?family=Muli',
  'public/lib/font-awesome/css/font-awesome.min.css',
  'public/lib/simple-line-icons/css/simple-line-icons.css',
  'public/lib/device-mockups/device-mockups.min.css'
],'public/css/homepage.css');
