<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Penyelenggara;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    protected function index()
    {
        return view('dashboard_penyelenggara');
    }

    protected function login() {
        return view('login_penyelenggara');
    }

    protected function list_event() {
        return view('dashboard_penyelenggara_listevent');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
}


?>
