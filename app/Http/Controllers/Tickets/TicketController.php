<?php
namespace App\Http\Controllers\Tickets;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Ticket;

class TicketController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    protected function index()
    {
        $data= Ticket::all();
        return view('welcome',['data'=>$data]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
}


?>
